.. internship23-docs documentation master file, created by
   sphinx-quickstart on Sun Dec 17 19:58:32 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Список проектов
===============

.. toctree::
   :maxdepth: 1

   projects/academy/index.md 
   projects/ds/index.md
   projects/lapa/index.md
   projects/ruts/index.md
   projects/skillas/index.md
   projects/tests_schoolars/index.md
   projects/tracker/index.md
   projects/volunteers/index.md
   projects/yoreactions/index.md
