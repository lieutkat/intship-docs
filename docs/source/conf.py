# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

import sys, os

sys.path.append(os.path.abspath('extentions'))

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'internship23'
copyright = '2023, Katerina K'
author = 'Katerina K'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    'myst_parser',
]
#source_suffix = ['.rst', '.md']

templates_path = ['_templates']
exclude_patterns = []

language = 'ru'

show_authors = True

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'furo'
html_static_path = ['_static']

html_baseurl = 'https://lieutkat.gitlab.io/intship-docs'

exclude_patterns = ['unpublished']

html_logo = '_static/tw_icon.jpg'
favicons = [
    {
        "rel": "icon",
        "static-file": "harappa_unicorn.jpg",
        "type": "image/jpg",
    },
]
